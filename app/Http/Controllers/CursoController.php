<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Curso;

class CursoController extends Controller
{
   
     
    public function index()
    {
        $cursos = Curso::all();

        return view('cursos.index', compact('cursos'));
    }

   
    public function create()
    {
        return view('cursos.create');
    }


    public function store(Request $request)
    {
        

        $curso = Curso::create($request->all());

        return redirect()->route('cursos.index')->with('success', 'Curso añadido a la lista.');
    }

   
    public function show($id)
    {
        $curso = curso::find($id);

        return view('cursos.show', compact('curso'));
    }

   
    public function edit($id)
    {
         $curso = Curso::find($id);
         return view('cursos.edit', compact('curso'));
    }

    
    public function update(Request $request, Curso $curso)
    {
        
        $curso->update($request->all());

        /*$curso->name = $request->name;
        $curso->descripcion = $request->descripcion;
        $curso->categoria = $request->categoria;
        
        $curso->save();*/

        return redirect()->route('cursos.index')->with('success', 'Curso actualizado');
    }

    
    public function destroy($id)
    {
        $curso = Curso::find($id)->delete();

        return redirect()->route('cursos.index')->with('success', 'Curso eliminado');
    }
}
