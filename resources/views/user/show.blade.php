@extends('layouts.app')

@section('content')
    <section class="content container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title h5">{{ $user->name }}</span>
                        </div>
                        
                    </div>

                    <div class="card-body">
                        
                        <div class="float-end">
                            <a class="btn btn-primary btn-sm" href="{{ route('users.index') }}"> Regresar</a>
                        </div>

                        <div>
                            <strong>Nombre de Usuario:</strong>
                            {{ $user->name }}
                        </div>

                       

                        <div class="form-group">
                            <strong>Correo Electrónico:</strong>
                            {{ $user->email }}
                        </div>

                        <div class="form-group">
                            <strong>Rol:</strong>
                            {{ $user->role }}
                        </div>

                        
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection