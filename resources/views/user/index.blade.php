@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Usuarios

                	@if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    
                </div>

                <div class="card-body shadow px-3">
   					<table class="table">
   						<caption>Lista de usuarios</caption>
					  <thead>
					    <tr>
					      <th scope="col">ID</th>	
					      <th scope="col">Usuario</th>
					      <th scope="col">Correo</th>
					    </tr>
					  </thead>
					  <tbody>
					@foreach($users as $user)
					    <tr>
					      <td>{{$user->id}}</td>
					      <td>{{$user->name}}</td>
					      <td>{{$user->email}}</td>
					      
					      <td>
					      	<form action="{{ route('users.destroy', $user->id )}}" method="POST">
					      		<a class="btn btn-sm btn-primary " href="{{route('users.show', $user->id)}}"><i class="fa fa-fw fa-eye"></i> Ver</a>
					      		<a class="btn btn-sm btn-primary " href="{{route('users.edit', $user->id)}}"><i class="fa fa-fw fa-eye"></i> Actualizar</a>
						    	@csrf
						    	@method('DELETE')
						    	<button type ="submit" class="btn btn-outline-danger btn-sm">Eliminar</button>
					    	</form>
						</td>
					    </tr>

					@endforeach
					  </tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection




