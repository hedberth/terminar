@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><strong>Cursos</strong>

                 <form action="{{route('cursos.store')}}" method="post">
                    @csrf
                  <div class="mb-3">
                    <label for="name" class="form-label">Nombre</label>
                    <input type="text" class="form-control" id="name" name="name">
                    
                </div>
                <div class="mb-3">
                    <label for="descripcion" class="form-label">Descrición</label>
                    <input type="text" class="form-control" id="descripcion" name="descripcion">
                </div>

                <div class="mb-3">
                    <label for="categoria" class="form-label">categoria</label>
                    <input type="text" class="form-control" id="categoria" name="categoria">
                </div>

                <button type="submit" class="btn btn-primary">Crear</button>
            </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection