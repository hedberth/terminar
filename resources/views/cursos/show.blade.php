@extends('layouts.app')

@section('content')
    <section class="content container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title h5">{{ $curso->name }}</span>
                        </div>
                        
                    </div>

                    <div class="card-body">
                        
                        <div class="float-end">
                            <a class="btn btn-primary btn-sm" href="{{ route('cursos.index') }}"> Regresar</a>
                        </div>

                        <div>
                            <strong>Nombre del curso:</strong>
                            {{ $curso->name }}
                        </div>

                       

                        <div class="form-group">
                            <strong>Descripción:</strong>
                            {{ $curso->descripcion }}
                        </div>


                        <div class="form-group">
                            <strong>Categoria:</strong>
                            {{ $curso->categoria }}
                        </div>

                        
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection