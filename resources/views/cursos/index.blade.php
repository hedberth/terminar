@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><strong>Cursos</strong>

                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                			<div class="float-right">
                                <a href="{{route('cursos.create')}}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Añadir Curso nuevo') }}
                                </a>
                            </div>
                    
                </div>

                <div class="card-body shadow px-3">
   					<table class="table">
   						<caption>Lista de cursos</caption>
					  <thead>
					    <tr>
					      <th scope="col">ID</th>	
					      <th scope="col">Curso</th>
					      <th scope="col">Categoria</th>
					    </tr>
					  </thead>
					<tbody>
                    @foreach($cursos as $curso)
                        <tr>
                          <td>{{$curso->id}}</td>
                          <td>{{$curso->name}}</td>
                          <td>{{$curso->categoria}}</td>
                          
                          <td>
                            <form action="{{ route('cursos.destroy', $curso->id )}}" method="POST">
                                <a class="btn btn-sm btn-primary " href="{{route('cursos.show', $curso->id)}}"><i class="fa fa-fw fa-eye"></i> Ver</a>
                                <a class="btn btn-sm btn-primary " href="{{route('cursos.edit', $curso->id)}}"><i class="fa fa-fw fa-eye"></i> Actualizar</a>
                                @csrf
                                @method('DELETE')
                                <button type ="submit" class="btn btn-outline-danger btn-sm">Eliminar</button>
                            </form>
                        </td>
                        </tr>

                    @endforeach
                      </tbody>

					</table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection